#include <stdio.h>
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <time.h>

#include "ffnt.h"
#include "io.h"
#include "scenarios.h"

#define DEG 8
#define MAXDEG 64

#ifdef __cplusplus
extern "C" {
#endif


int main(int argc, char ** argv)
{

    printf("\n===   FFNT DEMO   ===========================================================\n\n");


    // ---   Alloc arrays   ----------------------------------------------------

    // Integer polynomial (via coefficients; twice longer due to tests of negacyclic extension)
    const int32_t  p[MAXDEG] = {1,3,-2,0,   -4,-3,2,-1,   0,-2,-3,0,  -1,2,1,-3,    1,2,1,-2,   -2,4,-1,-4,    1,-4,-2,0,   -4,-1,-1,-4,
                                1,1,-4,-3,  -4,-1,4,-2,   3,-4,1,-2,   0,3,2,-4,   -4,3,-2,1,    3,-1,1,-3,    0,-3,-4,2,    0,-3,0,-1};
    const int32_t  q[2*DEG]  = {0,-2,-3,0,  -1,2,1,-3,    0,2,3,0,     1,-2,-1,3};   // negacyclic
    int64_t pqf[DEG], pqff[DEG];

    // FFT of p
    fp_t  fct_pr[MAXDEG], fct_pi[MAXDEG], fgs_pr[MAXDEG], fgs_pi[MAXDEG],  // FFT-CT/GS
            ifpr[MAXDEG], ifpi[MAXDEG];
    // FFNT of p
    fp_t  ffpr[ DEG],  ffpi[DEG],  // FFNT
            ffqr[ DEG],  ffqi[DEG],
            iffpr[DEG], iffpi[DEG],
            ffpqr[DEG], ffpqi[DEG];
    // FFT of negacyclic extension of p (i.e., naive approach for multiplication)
    fp_t  fpnr[ 2*DEG],   fpni[ 2*DEG],
            fqnr[ 2*DEG],   fqni[ 2*DEG],
            fpqnr[2*DEG],   fpqni[2*DEG];


    // ---   Prepare time measurement   ----------------------------------------

    struct timespec ts, te;
    int64_t elapsedtime;

#define TIMERCALL(func) { \
                            clock_gettime(CLOCK_REALTIME, &ts);                 \
                            func;                                               \
                            clock_gettime(CLOCK_REALTIME, &te);                 \
                            elapsedtime = te.tv_sec * INT64_C(1000000000) +     \
                                te.tv_nsec - (ts.tv_sec * INT64_C(1000000000) + \
                                ts.tv_nsec);                                    \
                        }


    // ---   Precompute omega's   ----------------------------------------------

    // (I)FFT
    FftTables * fft_omegas     = fft_init(DEG);
    FftTables * ifft_omegas    = ifft_init(DEG);
    // FFNT
    FftTables * fft_n_2_omegas = fft_init(DEG/2);
    FftTables * ifft_n_2_omegas= ifft_init(DEG/2);
    FftTables * fft_2n_omegas  = fft_init(2*DEG);
    FftTables * ifft_2n_omegas = ifft_init(2*DEG);

    // check
    if (fft_omegas == NULL ||
        ifft_omegas == NULL ||
        fft_n_2_omegas == NULL ||
        ifft_n_2_omegas == NULL ||
        fft_2n_omegas == NULL ||
        ifft_2n_omegas == NULL)
        return -1;


    // ===   Run scenarios   ===================================================

    printf("\n---   Timings   ----------------------------------------------------------------\n\n");

    // ---   FFT-CT(p), IFFT(p) on float   ----------------------------------------
    {
        // prepare floats: copy p, typecast to fp_t
        for (int i = 0; i < MAXDEG; i++)
        {
            fct_pr[i] = (fp_t)p[i];
            fct_pi[i] = (fp_t)0.0;
        }

        TIMERCALL(FFT_fp(fft_omegas, &fct_pr[0], &fct_pi[0]))

        printf("%-20s ... %ld ns\n", "FFT-CT on float", elapsedtime);

        // prepare for inverse
        for (int i = 0; i < MAXDEG; i++)
        {
            ifpr[i] = fct_pr[i];
            ifpi[i] = fct_pi[i];
        }

        TIMERCALL(IFFT_fp(ifft_omegas, &ifpr[0], &ifpi[0]))

        printf("%-20s ... %ld ns\n", "     inverse", elapsedtime);
    }

    // ---   FFT-GS(p), IFFT(p) on float   ----------------------------------------
    {
        // prepare floats: copy p, typecast to fp_t
        for (int i = 0; i < MAXDEG; i++)
        {
            fgs_pr[i] = (fp_t)p[i];
            fgs_pi[i] = (fp_t)0.0;
        }

        TIMERCALL(fft_transform_GS(fft_2n_omegas, &fgs_pr[0], &fgs_pi[0], true))

        printf("%-20s ... %ld ns\n", "FFT-GS on float", elapsedtime);

        //~ // prepare for inverse
        //~ for (int i = 0; i < MAXDEG; i++)
        //~ {
            //~ ifpr[i] = fgs_pr[i];
            //~ ifpi[i] = fgs_pi[i];
        //~ }

        //~ TIMERCALL(ifft_transform_GS(ifft_2n_omegas, &ifpr[0], &ifpi[0], true))

        //~ printf("%-20s ... %ld ns\n", "     inverse", elapsedtime);
    }

    // ---   FFT(p) on int   ---------------------------------------------------
    {
        TIMERCALL(FFT_int(fft_omegas, &p[0], &fct_pr[0], &fct_pi[0], DEG))

        printf("%-20s ... %ld ns\n", "FFT on int", elapsedtime);
    }

    // ---   FFT(q,-q) on float   ----------------------------------------------
    {
        // prepare floats: copy q, typecast to fp_t
        for (int i = 0; i < DEG; i++)
        {
            fqnr[i]          = (fp_t)q[i];
            fqnr[i+DEG]      = (fp_t)(-q[i]);
            fqni[i]          = (fp_t)0.0;
            fqni[i+DEG]      = (fp_t)0.0;
        }

        TIMERCALL(FFT_fp(fft_2n_omegas, &fqnr[0], &fqni[0]))

        printf("%-20s ... %ld ns\n", "FFT-neg on float", elapsedtime);
    }

    // ---   FFT(q,-q) on int   ------------------------------------------------
    {
        TIMERCALL(FFT_int(fft_2n_omegas, &q[0], &fqnr[0], &fqni[0], 2*DEG))

        printf("%-20s ... %ld ns\n", "FFT-neg on int", elapsedtime);
    }

    // ---   p * q via FFT on int   --------------------------------------------
    {
        TIMERCALL(poly_mult_FFT(fft_2n_omegas, ifft_2n_omegas,
                                &p[0], &q[0],
                                &fpnr[0], &fpni[0], &fqnr[0], &fqni[0], &fpqnr[0], &fpqni[0],
                                &pqf[0], DEG))

        printf("%-20s ... %ld ns\n", "p * q via FFT-neg", elapsedtime);
    }

    // ---   FFNT(p), IFFNT(p) on float   ----------------------------------------
    {
        // prepare floats: copy p, typecast to fp_t
        for (int i = 0; i < DEG; i++)
        {
            ffpr[i] = (fp_t)p[i];
            ffpi[i] = (fp_t)0.0;
        }

        TIMERCALL(ffnt_transform(fft_2n_omegas, fft_n_2_omegas, &ffpr[0], &ffpi[0]))

        printf("%-20s ... %ld ns\n", "FFNT on float", elapsedtime);

        // prepare for inverse
        for (int i = 0; i < DEG/2; i++)
        {
            iffpr[i] = ffpr[i];
            iffpi[i] = ffpi[i];
        }

        TIMERCALL(iffnt_transform(fft_2n_omegas, ifft_n_2_omegas, &iffpr[0], &iffpi[0]))

        printf("%-20s ... %ld ns\n", "     inverse", elapsedtime);
    }

    // ---   p * q via FFNT on int   -------------------------------------------
    {
        TIMERCALL(poly_mult_FFNT(fft_2n_omegas, fft_n_2_omegas, ifft_n_2_omegas,
                                 &p[0], &q[0],
                                 &ffpr[0], &ffpi[0], &ffqr[0], &ffqi[0], &ffpqr[0], &ffpqi[0],
                                 &pqff[0], DEG))

        printf("%-20s ... %ld ns\n", "p * q via FFNT", elapsedtime);
    }


    // ===   Print results   ===================================================

    printf("\n---   Results   ----------------------------------------------------------------\n\n");

    // p, q
    print_ary_int32("       p... = ", &p[0], MAXDEG);
    print_ary_int32("          p = ", &p[0], DEG);
    print_ary_int32("          q = ", &q[0], DEG);
    printf("\n");

    // FFT-CT(p) & IFFT(p)
    printf("Exp. FFT    = [(-4.000+0.000i), (8.536-0.950i), (-3.000-1.000i), (1.464-8.950i), (-2.000+0.000i), (1.464+8.950i), (-3.000+1.000i), (8.536+0.950i)]\n");
    print_ary_re_im("  FFT-CT(p) = ", &fct_pr[0], &fct_pi[0], DEG, 1.0);
    print_ary_re_im("  FFT-GS(p) = ", &fgs_pr[0], &fgs_pi[0], 2*DEG, 1.0);
    print_ary_re_im(" IFFT-CT(p) = ", &ifpr[0], &ifpi[0], DEG, 1.0);
    printf("\n");

    // FFT(p,-p)
    printf("Exp. FFT-nc = [(0.000+0.000i), (6.030+12.013i), (0.000+0.000i), (5.175-13.992i), (0.000+0.000i), (10.139+2.008i), (0.000+0.000i), (-13.344-3.988i), (0.000+0.000i), (-13.344+3.988i), (0.000+0.000i), (10.139-2.008i), (0.000+0.000i), (5.175+13.992i), (0.000+0.000i), (6.030-12.012i)]\n");
    print_ary_re_im("  FFT(p,-p) = ", &fpnr[0], &fpni[0], 2*DEG, 1.0);
    printf("\n");

    // p * q via FFT
    printf("Exp. p * q  = [11, -6, 3, -19, -4, 14, 24, 1]\n");
    print_ary_int64(" FFT p * q  = ", &pqf[0], DEG);
    printf("\n");

    // FFNT(p) & IFFNT(p)
    printf("Exp. FFNT   = [(3.015-6.006i), (2.588-6.996i), (-6.672-1.994i), (5.069-1.004i)]\n");
    print_ary_re_im("  FFNT(p)   = ", &ffpr[0],  &ffpi[0],  DEG/2, 1.0);
    print_ary_re_im(" IFFNT(p)   = ", &iffpr[0], &iffpi[0], DEG, 1.0);
    printf("\n");

    // p * q via FFNT
    printf("Exp. p * q  = [11, -6, 3, -19, -4, 14, 24, 1]\n");
    print_ary_int64("FFNT p * q  = ", &pqff[0], DEG);
    printf("\n");


    // ===   Free memory   =====================================================

    // (I)FFT
    tables_destroy( fft_omegas    );
    tables_destroy(ifft_omegas    );
    // FFNT
    tables_destroy( fft_n_2_omegas);
    tables_destroy(ifft_n_2_omegas);
    tables_destroy( fft_2n_omegas );
    tables_destroy(ifft_2n_omegas );


    return 0;
}

#ifdef __cplusplus
}
#endif
