#include <cstdio>
#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <ctime>

#include <iostream>
#include <fstream>

//~ #define BENCH_NTL
//~ #define BENCH_FFT_RED
//~ #define BENCH_MAX_COEFFS            // use MAXNORM as polynomial coeffs
//~ #define OUTPUT_POLYNOMIALS

#ifdef BENCH_NTL
#include <NTL/ZZ_pE.h>
NTL_CLIENT
#endif

#include "ffnt.h"
#include "io.h"
#include "scenarios.h"

#define NU_MIN      14
#define NU_MAX      18   // max 14
#define TAU         17   // max 17
#define MAXNORM     (1UL << TAU)
#define MEAS        10

#define FILEN_SZ        100
#define TIMING_BUFF_SZ  500


/** ===   Error estimates   ====================================================
 *
 *  ---   Error-free results   -------------------------------------------------
 *
 *  by (50) we have:
 *
 *      χ ≥ 2.54 ν + γ + τ + 2.19
 *
 *  for χ = 53 (IEEE double) and γ = τ (n.b., this is not a TFHE scenario, where γ < τ), we calculate
 *
 *  irb> 11.times{|i| nu = i+10; puts "nu = #{nu}, tau < #{(53 - 2.19 - 2.54*nu)/2}" }
 *
 *  ν = 10, τ < 12.705
 *  ν = 11, τ < 11.435
 *  ν = 12, τ < 10.165
 *  ν = 13, τ <  8.895
 *  ν = 14, τ <  7.625
 *  ν = 15, τ <  6.355
 *  ν = 16, τ <  5.085
 *  ν = 17, τ <  3.815
 *  ν = 18, τ <  2.545
 *  ν = 19, τ <  1.275
 *  ν = 20, τ <  0.005
 *
 *  ---   Error by 3σ rule   ---------------------------------------------------
 *
 *  by (51) we have:
 *
 *      χ ≥ 1.79 ν + γ + τ + 2
 *
 *  for χ = 53 (IEEE double) and γ = τ (n.b., this is not a TFHE scenario, where γ < τ), we calculate
 *
 *  irb> 11.times{|i| nu = i+10; puts "nu = #{nu}, tau < #{(53 - 2 - 1.79*nu)/2}" }
 *
 *  ν = 10, τ < 16.550
 *  ν = 11, τ < 15.655
 *  ν = 12, τ < 14.760
 *  ν = 13, τ < 13.865
 *  ν = 14, τ < 12.970
 *  ν = 15, τ < 12.075
 *  ν = 16, τ < 11.180
 *  ν = 17, τ < 10.285
 *  ν = 18, τ <  9.390
 *  ν = 19, τ <  8.495
 *  ν = 20, τ <  7.600
 *
 *  ========================================================================= */

#define NS_TO_MS    1000000

//~ #define VERBOSE

typedef struct res_times_t
{
    int64_t mult_ntl_ns;
    int64_t mult_fft_ns;
    int64_t mult_ffnt_ns;
} res_times_t;

typedef struct stats_t
{
    double ntl_avg_ms;
    double ntl_mean_ms;
    double ntl_stddev_ms;
    double fft_avg_ms;
    double fft_mean_ms;
    double fft_stddev_ms;
    double ffnt_avg_ms;
    double ffnt_mean_ms;
    double ffnt_stddev_ms;
} stats_t;

int cmp_ntl(const void * a, const void * b)
{
    return ((res_times_t *)a)->mult_ntl_ns - ((res_times_t *)b)->mult_ntl_ns;
}

int cmp_fft(const void * a, const void * b)
{
    return ((res_times_t *)a)->mult_fft_ns - ((res_times_t *)b)->mult_fft_ns;
}

int cmp_ffnt(const void * a, const void * b)
{
    return ((res_times_t *)a)->mult_ffnt_ns - ((res_times_t *)b)->mult_ffnt_ns;
}


int main(int argc, char ** argv)
{

    char timing_buff[TIMING_BUFF_SZ];
    char filename[FILEN_SZ], fn_suffix[FILEN_SZ];

    sprintf(fn_suffix, "fptype=%s__tau=%d__nu=%d-%d__meas=%d.txt",
#ifdef USE_LONG_DOUBLE
            "80-bit",
#else
            "64-bit",
#endif
            TAU, NU_MIN, NU_MAX, MEAS);

    std::ofstream tim_file, poly_file;
    sprintf(filename, "./out/timings_%s", fn_suffix);
    tim_file.open(filename, std::ios::trunc);   // rewrite existing file with new contents
    sprintf(filename, "./out/polynomials_%s", fn_suffix);
#ifdef OUTPUT_POLYNOMIALS
    poly_file.open(filename, std::ios::trunc);   // rewrite existing file with new contents
#endif

    tim_file << "\n\n===   FFNT Benchmark   =========================================================\n\n";
    tim_file << "N (degree) from   " << (1 << NU_MIN) << "   to   " << (1 << NU_MAX) << "   (2^" << NU_MIN << " ... 2^" << NU_MAX << ")\n";
    tim_file << "Max norm ‖p‖∞ ≤ 2^" << TAU << "\n";
    tim_file << MEAS << " measurements\n";
    tim_file << "Used floating-point type: " <<
#ifdef USE_LONG_DOUBLE
                "80-bit long double\n";
#else
                "64-bit double\n";
#endif


#ifdef BENCH_NTL
    // ---   Prepare NTL integers   --------------------------------------------

    // initiaze integer modular arithmetics mod 2^(2*tau + nu_max)
    ZZ_p::init(ZZ(1UL << (2*TAU + NU_MAX)));
#endif


    // ---   Prepare time measurement   ----------------------------------------

    srand(time(NULL));
    size_t i, j, nu;

    res_times_t ** tim  = (res_times_t **)malloc((NU_MAX - NU_MIN + 1) * sizeof(res_times_t *));
    for (i = 0; i <= NU_MAX - NU_MIN; i++)
        tim[i] = (res_times_t *)malloc(MEAS * sizeof(res_times_t));

    stats_t * stats = (stats_t *)malloc((NU_MAX - NU_MIN + 1) * sizeof(stats_t));

    struct timespec ts, te;
    int64_t elapsedtime, sumtime_ns;

#define TIMERCALL(func) { \
                            clock_gettime(CLOCK_REALTIME, &ts);                 \
                            func;                                               \
                            clock_gettime(CLOCK_REALTIME, &te);                 \
                            elapsedtime = te.tv_sec * INT64_C(1000000000) +     \
                                te.tv_nsec - (ts.tv_sec * INT64_C(1000000000) + \
                                ts.tv_nsec);                                    \
                        }


    // ===   Run scenarios   ===================================================

    printf("\n---   Running benchmarks   -----------------------------------------------------\n\n");
#ifdef OUTPUT_POLYNOMIALS
    poly_file << "res = {}\n";
#endif

    //DBG
    //~ fprintf(stderr, "\n@errh = {}\n");

    for (nu = NU_MIN; nu <= NU_MAX; nu++)
    {
        printf("nu = %2lu ", nu);
#ifdef OUTPUT_POLYNOMIALS
        poly_file << "\n# ---   nu = " << nu << "\n\n";
        poly_file << "puts \"nu = " << nu << "\"\nres[:nu_" << nu << "] = []\n";
#endif
        size_t deg = (1 << nu);

        //DBG
        //~ fprintf(stderr, "\nputs \"nu = %ld\"\n@errh[:nu_%ld] = []\nerr = @errh[:nu_%ld]\n", nu, nu, nu);


#ifdef BENCH_NTL
        // ---   Prepare NTL polynomials

        // initialize reduction polynomial
        ZZ_pX xNp1;
        //TODO use: inline ZZ_pX::ZZ_pX(INIT_MONO_TYPE, long i) { SetCoeff(*this, i); }
        SetCoeff(xNp1, deg);
        SetCoeff(xNp1, 0);

        // initialize polynomial modular arithmetics mod X^N + 1
        ZZ_pE::init(xNp1);

        // initialize NTL polynomials  mod X^N + 1
        ZZ_pE p_ntl, q_ntl, r_ntl;
#endif


        // ---   Alloc memory for polynomials and Fourier images

        int32_t * p     = (int32_t *)malloc(deg * sizeof(int32_t));
        int32_t * q     = (int32_t *)malloc(deg * sizeof(int32_t));

        int64_t * pqf   = (int64_t *)malloc(deg * sizeof(int64_t));
        int64_t * pqff  = (int64_t *)malloc(deg * sizeof(int64_t));

        fp_t * fpnr   = (fp_t *)malloc(2*deg * sizeof(fp_t));
        fp_t * fpni   = (fp_t *)malloc(2*deg * sizeof(fp_t));
        fp_t * fqnr   = (fp_t *)malloc(2*deg * sizeof(fp_t));
        fp_t * fqni   = (fp_t *)malloc(2*deg * sizeof(fp_t));
        fp_t * fpqnr  = (fp_t *)malloc(2*deg * sizeof(fp_t));
        fp_t * fpqni  = (fp_t *)malloc(2*deg * sizeof(fp_t));

        fp_t * ffpr    = (fp_t *)malloc(deg * sizeof(fp_t));
        fp_t * ffpi    = (fp_t *)malloc(deg * sizeof(fp_t));
        fp_t * ffqr    = (fp_t *)malloc(deg * sizeof(fp_t));
        fp_t * ffqi    = (fp_t *)malloc(deg * sizeof(fp_t));
        fp_t * ffpqr   = (fp_t *)malloc(deg * sizeof(fp_t));
        fp_t * ffpqi   = (fp_t *)malloc(deg * sizeof(fp_t));


        // ---   Precompute omega's
        //TODO measure time?

        FftTables *  fft_n_2_omegas =  fft_init(deg/2);
        FftTables * ifft_n_2_omegas = ifft_init(deg/2);
        FftTables *  fft_2n_omegas  =  fft_init(2*deg);
        FftTables * ifft_2n_omegas  = ifft_init(2*deg);

        // check   // fft_omegas == NULL ||
        if (fft_n_2_omegas  == NULL ||
            ifft_n_2_omegas == NULL ||
            fft_2n_omegas   == NULL ||
            ifft_2n_omegas  == NULL)
            return -1;


        // ---   Run measurements   --------------------------------------------

        for (i = 0; i < MEAS; i++)
        {
            // ---   FFT(p,-p) on float
            {//TODO
            //~ // prepare floats: copy p, typecast to fp_t
            //~ for (int i = 0; i < DEG; i++)
            //~ {
                //~ fpnr[i]          = (fp_t)p[i];
                //~ fpnr[i + DEG] = (fp_t)(-p[i]);
                //~ fpni[i]          = (fp_t)0.0;
                //~ fpni[i + DEG] = (fp_t)0.0;
            //~ }

            //~ TIMERCALL(FFT_fp(fft_nc_omegas, &fpnr[0], &fpni[0]))

            //~ printf("%-20s ... %ld ns\n", "FFT-neg on float", elapsedtime);
            }

            // ---   FFT(p,-p) on int
            {//TODO
            //~ TIMERCALL(FFT_int(fft_nc_omegas, &p[0], &fpnr[0], &fpni[0], 2*DEG))

            //~ printf("%-20s ... %ld ns\n", "FFT-neg on int", elapsedtime);
            }

            printf(".");fflush(stdout);

#ifdef BENCH_NTL
            // ---   p * q via NTL
            {
                // fill polynomials with random values
                random(p_ntl);
                random(q_ntl);

                TIMERCALL(mul(r_ntl, p_ntl, q_ntl))

                tim[nu - NU_MIN][i].mult_ntl_ns = elapsedtime;
            }
            printf(":");fflush(stdout);
#endif

#ifdef BENCH_FFT_RED
            // ---   p * q via FFT on int
            {
                // fill polynomials with random values
                for (j = 0; j < deg; j++)
                {
#ifdef BENCH_MAX_COEFFS
                    p[j] = MAXNORM - 1;
                    q[j] = MAXNORM - 1;
#else
                    p[j] = rand() % (2 * MAXNORM) - MAXNORM;
                    q[j] = rand() % (2 * MAXNORM) - MAXNORM;
#endif
                }

                TIMERCALL(poly_mult_FFT(fft_2n_omegas, ifft_2n_omegas,
                                        &p[0], &q[0],
                                        &fpnr[0], &fpni[0], &fqnr[0], &fqni[0], &fpqnr[0], &fpqni[0],
                                        &pqf[0], deg))

                tim[nu - NU_MIN][i].mult_fft_ns = elapsedtime;
            }
            printf("|");fflush(stdout);
#endif

            // ---   p * q via FFNT on int
            {
                // fill polynomials with random values
#ifdef OUTPUT_POLYNOMIALS
                poly_file << "p = PolyN.coeffs([";
#endif
                for (j = 0; j < deg; j++)
                {
#ifdef BENCH_MAX_COEFFS
                    p[j] = MAXNORM - 1;
#else
                    p[j] = rand() % (2 * MAXNORM) - MAXNORM;
#endif
#ifdef OUTPUT_POLYNOMIALS
                    poly_file << p[j] << ",";
#endif
                }
#ifdef OUTPUT_POLYNOMIALS
                poly_file <<  "], " << TAU << ")\nq = PolyN.coeffs([";
#endif
                for (j = 0; j < deg; j++)
                {
#ifdef BENCH_MAX_COEFFS
                    q[j] = MAXNORM - 1;
#else
                    q[j] = rand() % (2 * MAXNORM) - MAXNORM;
#endif
#ifdef OUTPUT_POLYNOMIALS
                    poly_file << q[j] << ",";
#endif
                }
#ifdef OUTPUT_POLYNOMIALS
                poly_file <<  "], " << TAU << ")\npq = PolyN.coeffs([";
#endif

                TIMERCALL(poly_mult_FFNT(fft_2n_omegas, fft_n_2_omegas, ifft_n_2_omegas,
                                         &p[0], &q[0],
                                         &ffpr[0], &ffpi[0], &ffqr[0], &ffqi[0], &ffpqr[0], &ffpqi[0],
                                         &pqff[0], deg))

                tim[nu - NU_MIN][i].mult_ffnt_ns = elapsedtime;

#ifdef OUTPUT_POLYNOMIALS
                for (j = 0; j < deg; j++)
                {
                    poly_file << pqff[j] << ",";
                }
                poly_file << "])\nr = p.fast_mult q\nres[:nu_" << nu << "] << (r == pq)\n";
                poly_file <<  "print res[:nu_" << nu << "].last ? \"o\" : \"X\"\n";
#endif
            }
        }
#ifdef OUTPUT_POLYNOMIALS
        poly_file <<  "puts\n";
#endif
        printf("\n");


        // ---   Calc statistics

#ifdef BENCH_NTL
        // avg NTL
        sumtime_ns = 0;
        for (i = 0; i < MEAS; i++)
            sumtime_ns += tim[nu - NU_MIN][i].mult_ntl_ns;
        stats[nu - NU_MIN].ntl_avg_ms = (double)sumtime_ns / MEAS / NS_TO_MS;
        // mean NTL
        qsort(&tim[nu - NU_MIN][0], MEAS, sizeof(res_times_t), cmp_ntl);
        stats[nu - NU_MIN].ntl_mean_ms = (double)tim[nu - NU_MIN][MEAS/2].mult_ntl_ns / NS_TO_MS;
#endif

#ifdef BENCH_FFT_RED
        // avg FFT
        sumtime_ns = 0;
        for (i = 0; i < MEAS; i++)
            sumtime_ns += tim[nu - NU_MIN][i].mult_fft_ns;
        stats[nu - NU_MIN].fft_avg_ms = (double)sumtime_ns / MEAS / NS_TO_MS;
        // mean FFT
        qsort(&tim[nu - NU_MIN][0], MEAS, sizeof(res_times_t), cmp_fft);
        stats[nu - NU_MIN].fft_mean_ms = (double)tim[nu - NU_MIN][MEAS/2].mult_fft_ns / NS_TO_MS;
#endif

        // avg FFNT
        sumtime_ns = 0;
        for (i = 0; i < MEAS; i++)
            sumtime_ns += tim[nu - NU_MIN][i].mult_ffnt_ns;
        stats[nu - NU_MIN].ffnt_avg_ms = (double)sumtime_ns / MEAS / NS_TO_MS;
        // mean FFNT
        qsort(&tim[nu - NU_MIN][0], MEAS, sizeof(res_times_t), cmp_ffnt);
        stats[nu - NU_MIN].ffnt_mean_ms = (double)tim[nu - NU_MIN][MEAS/2].mult_ffnt_ns / NS_TO_MS;


#ifdef VERBOSE

        // ---   Print results

        printf("\n---   Results   ----------------------------------------------------------------\n\n");

        // p
        print_ary_int("          p = [", &p[0], DEG);
        print_ary_int("          q = [", &q[0], DEG);
        printf("\n");

        //~ // FFT(p,-p)
        //~ printf("Exp. FFT-nc = [(0.000+0.000i), (6.030+12.013i), (0.000+0.000i), (5.175-13.992i), (0.000+0.000i), (10.139+2.008i), (0.000+0.000i), (-13.344-3.988i), (0.000+0.000i), (-13.344+3.988i), (0.000+0.000i), (10.139-2.008i), (0.000+0.000i), (5.175+13.992i), (0.000+0.000i), (6.030-12.012i)]\n");
        //~ print_ary_re_im("  FFT(p,-p) = ", &fpnr[0], &fpni[0], 2*DEG, 1.0);
        //~ printf("\n");

        // p * q via FFT
        //~ printf("Exp. p * q  = [11, -6, 3, -19, -4, 14, 24, 1]\n");
        print_ary_int("     p * q  = [", &pqf[0], DEG);

        // p * q via FFNT
        //~ printf("Exp. p * q  = [11, -6, 3, -19, -4, 14, 24, 1]\n");
        print_ary_int("     p * q  = [", &pqff[0], DEG);

        printf("\n");

#endif   // #ifdef VERBOSE


        // ---   Free arrays & tables

        free(p    );
        free(q    );
        free(pqf  );
        free(pqff );

        free(fpnr );
        free(fpni );
        free(fqnr );
        free(fqni );
        free(fpqnr);
        free(fpqni);

        free(ffpr );
        free(ffpi );
        free(ffqr );
        free(ffqi );
        free(ffpqr);
        free(ffpqi);

        tables_destroy( fft_n_2_omegas);
        tables_destroy(ifft_n_2_omegas);
        tables_destroy( fft_2n_omegas );
        tables_destroy(ifft_2n_omegas );
    }


    // ===   Print timings & results   =========================================

    tim_file << "\n---   Timings   ----------------------------------------------------------------\n\n";
    tim_file << " Degree |  Mult via NTL: MEAN |  Mult via FFT: MEAN | Mult via FFNT: MEAN | Mean speedup (FFNT over FFT)\n";
    tim_file << "---------------------------------------------------------------------------------------------------------\n";

    for (nu = NU_MIN; nu <= NU_MAX; nu++)
    {
        sprintf(timing_buff, "   2^%2lu |         %8.3f ms |         %8.3f ms |         %8.3f ms |       %6.2fx\n",
                nu,
                stats[nu - NU_MIN].ntl_mean_ms,    // stats[nu - NU_MIN].ntl_mean_ms,
                stats[nu - NU_MIN].fft_mean_ms,    // stats[nu - NU_MIN].fft_mean_ms,
                stats[nu - NU_MIN].ffnt_mean_ms,   //
                stats[nu - NU_MIN].fft_mean_ms / stats[nu - NU_MIN].ffnt_mean_ms);
        tim_file << timing_buff;
    }


    // ---   Free memory & close files

    for (i = 0; i <= NU_MAX - NU_MIN; i++)
        free(tim[i]);
    free(tim);
    free(stats);

    tim_file.close();


    return 0;
}
