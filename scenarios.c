#include <stddef.h>
#include <stdint.h>
#include <math.h>

#include "ffnt.h"
#include "scenarios.h"

//DBG
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif


// ===   FFT   =================================================================

void FFT_int(const FftTables *const fft_tables,
             const int32_t *const f_int,
             fp_t *const f_fpr,
             fp_t *const f_fpi,
             const size_t f_size)
{
    for (size_t i = 0; i < f_size; i++)
    {
        f_fpr[i] = (fp_t)f_int[i];
        f_fpi[i] = (fp_t)0.0;
    }

    fft_transform(fft_tables, f_fpr, f_fpi);
}

void FFT_fp(const FftTables *const fft_tables,
            fp_t *const f_fpr,
            fp_t *const f_fpi)
{
    fft_transform(fft_tables, f_fpr, f_fpi);
}

void IFFT_fp(const FftTables *const ifft_tables,
             fp_t *const f_fpr,
             fp_t *const f_fpi)
{
    ifft_transform(ifft_tables, f_fpr, f_fpi);
}


// ===   Multiplication   ======================================================

void poly_mult_FFT(const FftTables *const fft_tables,
                   const FftTables *const ifft_tables,
                   const int32_t *const p_int,
                   const int32_t *const q_int,
                   fp_t *const fpr,
                   fp_t *const fpi,
                   fp_t *const fqr,
                   fp_t *const fqi,
                   fp_t *const fpqr,
                   fp_t *const fpqi,
                   int64_t *const pq_int,
                   const size_t p_size)
{
    // typecast to fp_t & prepare negacyclic extension
    for (size_t i = 0; i < p_size; i++)
    {
        fpr[i]          = (fp_t)p_int[i];
        fpr[i+p_size]   = (fp_t)(-p_int[i]);
        fqr[i]          = (fp_t)q_int[i];
        fqr[i+p_size]   = (fp_t)(-q_int[i]);
        fpi[i]          = 0.0;
        fpi[i+p_size]   = 0.0;
        fqi[i]          = 0.0;
        fqi[i+p_size]   = 0.0;
    }

    // FFT (via GS without bit-reverse, bit-reverse must be considered
    //      during dyadic multiplication to skip zeros & conjugates
    //      at correct positions)
    fft_transform_GS(fft_tables, fpr, fpi, false);
    fft_transform_GS(fft_tables, fqr, fqi, false);

    // option B:
    // FFT (via GS with bit-reverse in order to make it easy
    //      for skipping zeros & conjugates in dyadic multiplication)
    //~ fft_transform_GS(fft_tables, fpr, fpi, true);
    //~ fft_transform_GS(fft_tables, fqr, fqi, true);

    const size_t *const bitrevtab = fft_tables->bit_reversed;

    // dyadic complex multiplication
    for (size_t i = 1; i < p_size; i += 2)
    {
        size_t bri = bitrevtab[i];
        size_t brimo = bitrevtab[i-1];
            // option B
            //~ size_t bri = i;
            //~ size_t brimo = i - 1;
        // 1st half
        fpqr[brimo] = 0.0;
        fpqi[brimo] = 0.0;
        fpqr[bri] = fpr[bri] * fqr[bri] - fpi[bri] * fqi[bri];
        fpqi[bri] = fpr[bri] * fqi[bri] + fpi[bri] * fqr[bri];

        // 2nd half ... conjugate
        size_t bri2p = bitrevtab[2*p_size - i];
        size_t bri2pmo = bitrevtab[2*p_size - i - 1];
            // option B
            //~ size_t bri2p = 2*p_size - i;
            //~ size_t bri2pmo = 2*p_size - i - 1;
        fpqr[bri2p] =  fpqr[bri];
        fpqi[bri2p] = -fpqi[bri];
        fpqr[bri2pmo] = 0.0;
        fpqi[bri2pmo] = 0.0;
    }

    // IFFT (via CT WITH bit-reverse)
    ifft_transform_CT(ifft_tables, fpqr, fpqi, false);
    // option B
    //~ ifft_transform_CT(ifft_tables, fpqr, fpqi, true);

    // scale by 0.5 & round to integers (1st half only)
    for (size_t i = 0; i < p_size; i++)
    {
        pq_int[i] = (int64_t)round(fpqr[i] * 0.5);
    }
}

void poly_mult_FFNT(const FftTables *const ffnt_2n_tables,
                    const FftTables *const fft_n_2_tables,
                    const FftTables *const ifft_n_2_tables,
                    const int32_t *const p_int,
                    const int32_t *const q_int,
                    fp_t *const fpr,
                    fp_t *const fpi,
                    fp_t *const fqr,
                    fp_t *const fqi,
                    fp_t *const fpqr,
                    fp_t *const fpqi,
                    int64_t *const pq_int,
                    const size_t p_size)
{
    // typecast to fp_t
    for (size_t i = 0; i < p_size; i++)
    {
        fpr[i] = (fp_t)p_int[i];
        fqr[i] = (fp_t)q_int[i];
    }

    // FFNT (via GS without bit-reverse)
    ffnt_transform(ffnt_2n_tables, fft_n_2_tables, fpr, fpi);
    ffnt_transform(ffnt_2n_tables, fft_n_2_tables, fqr, fqi);

    // dyadic complex multiplication
    for (size_t i = 0; i < p_size/2; i++)
    {
        // Re
        fpqr[i] = fpr[i] * fqr[i] - fpi[i] * fqi[i];
        // Im
        fpqi[i] = fpi[i] * fqr[i] + fpr[i] * fqi[i];
    }

    // IFFNT (via CT without bit-reverse)
    iffnt_transform(ffnt_2n_tables, ifft_n_2_tables, fpqr, fpqi);

    // round to integers
    for (size_t i = 0; i < p_size; i++)
    {
        //DBG
//~ #ifdef USE_LONG_DOUBLE
        //~ fprintf(stderr, "err << %.8Lf\n", (fpqr[i] > round(fpqr[i])) ? (fpqr[i] - round(fpqr[i])) : (round(fpqr[i]) - fpqr[i]));
//~ #else
        //~ fprintf(stderr, "err << %.6f\n", (fpqr[i] > round(fpqr[i])) ? (fpqr[i] - round(fpqr[i])) : (round(fpqr[i]) - fpqr[i]));
//~ #endif

        pq_int[i] = (int64_t)round(fpqr[i]);
    }
}

#ifdef __cplusplus
}
#endif
