#ifndef IO_H
#define IO_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stddef.h>

#include "ffnt.h"

/*******************************************************************************
 * Print the +intro+ string followed by an array of +int32_t+ elements.
 * */
void print_ary_int32(const char *const intro,
                     const int32_t *const ary,
                     const size_t size);

/*******************************************************************************
 * Print the +intro+ string followed by an array of +int64_t+ elements.
 * */
void print_ary_int64(const char *const intro,
                     const int64_t *const ary,
                     const size_t size);

/*******************************************************************************
 * Print the +intro+ string followed by an array of complex numbers
 * given in two arrays +re+ and +im+ of type +fp_t+, multiplied
 * by the +mult+ parameter.
 * */
void print_ary_re_im(const char *const intro,
                     const fp_t *const re,
                     const fp_t *const im,
                     const size_t size,
                     const fp_t mult);

#ifdef __cplusplus
}
#endif

#endif
