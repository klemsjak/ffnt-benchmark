#ifndef SCENARIOS_H
#define SCENARIOS_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdint.h>


// ===   FFT   =================================================================

/*******************************************************************************
 * FFT on integer input.
 *
 * Casts integers to floating-points & runs FFT.
 *
 * */
void FFT_int(const FftTables *const fft_tables,
             const int32_t *const f_int,
             fp_t *const f_fpr,
             fp_t *const f_fpi,
             const size_t f_size);

/*******************************************************************************
 * FFT on floating-point input.
 *
 * Just runs FFT.
 *
 * */
void FFT_fp(const FftTables *const fft_tables,
            fp_t *const f_fpr,
            fp_t *const f_fpi);

/*******************************************************************************
 * Inverse FFT on floating-point input.
 *
 * Just runs inverse FFT.
 *
 * */
void IFFT_fp(const FftTables *const ifft_tables,
             fp_t *const f_fpr,
             fp_t *const f_fpi);


// ===   Negacyclic Polynomial Multiplication   ================================

/*******************************************************************************
 * Negacyclic polynomial multiplication, redundant approach.
 *
 * This scenario is implemented in the TFHE Library
 * (https://github.com/tfhe/tfhe), it proceeds as follows:
 *
 * 1) extend polynomials by their negacyclic projection (i.e., [p] -> [p, -p]),
 * 2) FFT extended polynomials
 * 3) multiply FFT images dyadically
 * 4) IFFT
 * 5) scale by 0.5 & round to integers
 *
 * */
void poly_mult_FFT(const FftTables *const fft_tables,
                   const FftTables *const ifft_tables,
                   const int32_t *const p_int,
                   const int32_t *const q_int,
                   fp_t *const fpr,
                   fp_t *const fpi,
                   fp_t *const fqr,
                   fp_t *const fqi,
                   fp_t *const fpqr,
                   fp_t *const fpqi,
                   int64_t *const pq_int,
                   const size_t p_size);

/*******************************************************************************
 * Negacyclic polynomial multiplication, non-redundant approach.
 *
 * This is a novel scenario using FFNT, it proceeds as follows:
 *
 * 1) FFNT polynomials
 * 2) multiply FFNT images dyadically
 * 3) IFFNT
 * 4) round to integers
 *
 * */
void poly_mult_FFNT(const FftTables *const ffnt_2n_tables,
                    const FftTables *const fft_n_2_tables,
                    const FftTables *const ifft_n_2_tables,
                    const int32_t *const p_int,
                    const int32_t *const q_int,
                    fp_t *const fpr,
                    fp_t *const fpi,
                    fp_t *const fqr,
                    fp_t *const fqi,
                    fp_t *const fpqr,
                    fp_t *const fpqi,
                    int64_t *const pq_int,
                    const size_t p_size);

#ifdef __cplusplus
}
#endif

#endif   // SCENARIOS_H
