#include <stdio.h>
#include <stdint.h>
#include <stddef.h>

#include "ffnt.h"
#include "io.h"

#ifdef __cplusplus
extern "C" {
#endif

void print_ary_int32(const char *const intro,
                     const int32_t *const ary,
                     const size_t size)
{
    printf("%s[", intro);
    for (size_t i = 0; i < size-1; i++)
        printf("%d, ", ary[i]);
    printf("%d]\n", ary[size-1]);
}

void print_ary_int64(const char *const intro,
                     const int64_t *const ary,
                     const size_t size)
{
    printf("%s[", intro);
    for (size_t i = 0; i < size-1; i++)
        printf("%ld, ", ary[i]);
    printf("%ld]\n", ary[size-1]);
}

void print_ary_re_im(const char *const intro,
                     const fp_t *const re,
                     const fp_t *const im,
                     const size_t size,
                     const fp_t mult)
{
    printf("%s[", intro);
#ifdef USE_LONG_DOUBLE
    for (size_t i = 0; i < size-1; i++)
        printf("(%.4Lf%+.4Lfi), ", re[i] * mult, im[i] * mult);
    printf("(%.4Lf%+.4Lfi)]\n", re[size-1] * mult, im[size-1] * mult);
#else
    for (size_t i = 0; i < size-1; i++)
        printf("(%.3f%+.3fi), ", re[i] * mult, im[i] * mult);
    printf("(%.3f%+.3fi)]\n", re[size-1] * mult, im[size-1] * mult);
#endif
}

#ifdef __cplusplus
}
#endif
