
.DEFAULT_GOAL := demo

#	the following flags are suggested as the best
#	at https://www.nayuki.io/page/fast-fourier-transform-in-x86-assembly
#~ CFLAGS = -O2 -march=native -mtune=native -ftree-vectorize -Wall
#	however, by experiment, the fastest is simply O1/O2 (negligible difference)
CC       = gcc
CFLAGS   = -O2 -g -Wall -Wno-format-overflow -march=native

CXX      = g++
CXXFLAGS = -O2 -g -Wall -Wno-format-overflow -march=native -std=c++11 -pthread

#	include paths
INCPATHS = -I/home/fakub/documents/repos-phd/ffnt/install/include -Iinclude

#	library paths
LIBPATHS = -L/home/fakub/documents/repos-phd/ffnt/install/lib

#	libraries
LIBS     = -lntl -lm -lgmp

FFT_BASE_CC_OBJS     = scenarios.cc.o  io.cc.o  ffnt.cc.o
FFT_BASE_CXX_OBJS    = scenarios.cxx.o io.cxx.o ffnt.cxx.o

#	ordinary object files
%.cc.o: %.c
	$(CC) $(CFLAGS)    -c $(INCPATHS) $< -o $@

%.cxx.o: %.c
	$(CXX) $(CXXFLAGS) -c $(INCPATHS) $< -o $@

demo: $(FFT_BASE_CC_OBJS) demo.cc.o
	$(CC) $(CFLAGS)    $(FFT_BASE_CC_OBJS)  demo.cc.o   -lm -o $@

benchmark: $(FFT_BASE_CXX_OBJS)
	$(CXX) $(CXXFLAGS) -c $(INCPATHS) benchmark.cpp
	$(CXX) $(CXXFLAGS) $(FFT_BASE_CXX_OBJS) benchmark.o $(LIBPATHS) $(LIBS) -o $@

all: demo benchmark

.PHONY: clean
clean:
	rm -f *.o
	rm -f demo benchmark
