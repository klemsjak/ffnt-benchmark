

===   FFNT Benchmark   =========================================================

N (degree) from   16384   to   262144   (2^14 ... 2^18)
Max norm ‖p‖∞ ≤ 2^17
1000 measurements
Used floating-point type: 80-bit long double

---   Timings   ----------------------------------------------------------------

 Degree | Mult via FFNT: MEAN
------------------------------
   2^14 |            1.818 ms
   2^15 |            3.825 ms
   2^16 |            8.407 ms
   2^17 |           18.235 ms
   2^18 |           40.899 ms
